import React from "react";
import { useParams } from "react-router-dom";

import Language from "../pages/Language";

function ViewLanguage() {
  const { id } = useParams();
  return (
    <>
      <Language id={id}></Language>
    </>
  );
}

export default ViewLanguage;
