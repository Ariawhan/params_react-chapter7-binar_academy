import React, { Component } from "react";
import { Link } from "react-router-dom";

class LanguageComponent extends Component {
  state = { name: this.props.name, images: this.props.image };
  render() {
    return (
      <>
        <div className="language-item">
          <div className="language-name">{this.state.name}</div>
          {console.log(this.state.images)}
          <img
            className="language-image"
            src={this.state.images}
            alt={Math.floor(Math.random() * 200)}
          />
        </div>
        {this.props.isBtn ? (
          <Link to={`/language/${this.state.name}`}>
            <button>View</button>
          </Link>
        ) : (
          <></>
        )}
      </>
    );
  }
}

export default LanguageComponent;
